
var fb = require('./firebase.js');
var SerialPort = require('serialport');
var port = new SerialPort('/dev/ttyUSB0', {
    baudRate: 9600
});

port.on('open', onOpen);
port.on('data', onData);

function onOpen() {
    console.log("BBB <-> USB Serial Connection Established");
}

function onData(data) {
    var aaa = data.toString();
    console.log(aaa);
    if (aaa.search("karma_lup_0") != -1) {
        console.log("Üstü Kapat");
        fb.updateState(1, false);
    } else if (aaa.search("karma_lup_1") != -1) {
        console.log("Üstü Aç");
        fb.updateState(1, true);
    } else if (aaa.search("karma_ldown_0") != -1) {
        console.log("Altı Kapat");
        fb.updateState(2, false);
    } else if (aaa.search("karma_ldown_1") != -1) {
        console.log("Altı Aç");
        fb.updateState(2, true);
    } else if (aaa.search("karma_lall_0") != -1) {
        console.log("Hepsini Kapat");
        fb.updateState(1, false);
        fb.updateState(2, false);
    } else if (aaa.search("karma_lall_1") != -1) {
        console.log("Hepsini Aç");
        fb.updateState(1, true);
        fb.updateState(2, true);
    } else {}

    aaa = "";

}

