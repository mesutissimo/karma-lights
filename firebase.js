'use strict';
var ops = require('./operations.js');
var firebase = require("firebase-admin");
var serviceAccount = require("./secure/automation-karma-firebase-adminsdk-bh9vf-abe0d63fb8.json");

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://automation-karma.firebaseio.com"
});

var db = firebase.database();

var lightsRef = db.ref('/lights/');
var eventsRef = db.ref('/last_event/');
var autoCloseRef = db.ref('/auto_close/');
// Get actual values first

lightsRef.once('value', function (snapshot) {
    snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        console.log(childKey);
        console.log(childData);
        // Apply the last states of lights  TODO Must be all devices
        ops.lightChange(childKey, childData);
    })
});

lightsRef.on('child_changed', function (data) {
    console.log(data.key, data.val());
    ops.lightChange(data.key, data.val());
});

module.exports = {

    autoCloseEvent: function () {

        autoCloseRef.child(Date.now()).set(getDateT() + " Auto Close Job")

    },


    updateState: function (l, s) {

        // Işıkların durumunu değiştirir.

        lightsRef.update({
            ['light' + l]: s
        });
// Işıkların son durumunu olay listesine yazar.

        eventsRef.child(Date.now()).set({
            light: l,
            action: s,
            timestamp: getDateT()
        })

    },

    checkState: function () {
        var lightsd = [];

        lightsRef.once('value', function (snapshot) {

            snapshot.forEach(function (childSnapshot) {
                var childKeya = childSnapshot.key;
                var childDataa = childSnapshot.val();

                lightsd[childKeya] = childDataa;
            })
        });

        return lightsd;

    }


};

function getDateT() {
    var event = new Date(Date.now());
    return event.toString();
}