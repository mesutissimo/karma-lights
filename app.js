//Loading modules
let express = require('express');
let favicon = require('express-favicon');
let path = require("path");
//use favicon icon path to access in application.
let app = express();
app.use(favicon(__dirname + '/images/favicon.png'));
let server = require('http').createServer(app);
let io = require('socket.io')(server);
let b = require('bonescript');
//let db = require('./mysqlconn.js');
let fb = require('./firebase.js');
let mailer = require('./email_manager');
// let fab = require('./serial.js');

app.use(favicon(path.join(__dirname+'/images/favicon.ico')));

app.use(express.static(__dirname + '/node_modules'));
app.use(express.static(__dirname + '/css'));
app.use(express.static(__dirname + '/js'));
app.use(express.static(__dirname + '/images'));

app.get('/', function(req, res,next) {
    res.sendFile(__dirname + '/index.html');
});

server.listen(8888);
// When communication is established
let lights = [];


io.on('connection', function (socket) {
  socket.on('changeState', handleChangeState);
  socket.on('lastStatus', handleCheckState);
  socket.on('sendMail', sendHelpMail);
  socket.emit('lastStatus', {
      a : lights
  });


});
//
// Change led state when a button is pressed
function handleChangeState(data) {
    //let newData = JSON.parse(data);
    let l = data.light;
    let s = data.state;
    //console.log(data);
    //*console.log("LED = " + newData.state);
    // turns the LED ON or OFF
    console.log(data);
    console.log(l);
    console.log(s);
    fb.updateState(l,s);
}

function handleCheckState() {
    lights = fb.checkState();
    console.log("asd");
    console.log(lights);
    return lights;
}

// Displaying a console message for user feedback
server.listen(console.log("Server Running ..."));

// Starting the cronjob.
autoCloseLights();

function autoCloseLights() {
    let CronJob = require('cron').CronJob;
    new CronJob('0 0 23 * * *', function() {
        console.log('Auto Light Closer Done');
        mailer.sendMail_AutoCloseLights();
        fb.autoCloseEvent();
    }, null, true, 'Europe/Istanbul');


}


function sendHelpMail(data) {
    mailer.sendMail_Custom(data.user + " diyorki : " + data.email + " gibi bir durum var.");
    console.log("Web Yardım Mesajı Alındı : " + data.user + " " + data.email);
}


// Sunucu çalıştı epostasını gönder :

// mailer.sendMail_ServerStarted();
